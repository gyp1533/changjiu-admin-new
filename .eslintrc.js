// https://eslint.org/docs/user-guide/configuring

module.exports = {
    root: true,
    parserOptions: {
        parser: 'babel-eslint'
    },
    env: {
        browser: true,
    },
    extends: [
        // https://github.com/vuejs/eslint-plugin-vue#priority-a-essential-error-prevention
        // consider switching to `plugin:vue/strongly-recommended` or `plugin:vue/recommended` for stricter rules.
        'plugin:vue/essential',
        // https://github.com/standard/standard/blob/master/docs/RULES-en.md
        'standard'
    ],
    // required to lint *.vue files
    plugins: [
        'vue'
    ],
    // 自定义规则,0代表关闭该规则
    rules: {
        // 生成器函数*的前后空格
        'generator-star-spacing': 'off',
        // development环境允许debugger
        'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
        //引号类型 `` "" ''
        "quotes": 0,
        // 对象字面量中的属性名是否强制双引号
        "quote-props": 0,
        // 缩进风格，2个空格
        "indent": [2, 4],
        // 对象字面量中冒号的前后空格
        "key-spacing": 0,
        // 控制逗号前后的空格
        "comma-spacing": 0,
        //可以使用tab
        "no-tabs": 0,
        //可以混用tab和空格
        "no-mixed-spaces-and-tabs": [0],
        // 大括号后面不能出现空行--关闭
        "padded-blocks": 0,
        //自闭合的标签允许有end标签
        "vue/no-parsing-error": [2, {
            "x-invalid-end-tag": false
        }],
        //对象大括号两侧的空格
        "standard/object-curly-even-spacing": 0
    }
}
