const BASE_URL = process.env.NODE_ENV === 'production'
  ? '/'
  : 'http://39.107.6.117:8082/'

  module.exports = {
    baseUrl: BASE_URL
}