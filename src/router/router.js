import Main from '@/views/Main.vue';
import { ensure } from '@/router/ensure';

// 不作为Main组件的子页面展示的页面单独写，如下
export const loginRouter = {
    path: '/login',
    name: 'login',
    meta: {
        title: 'Login - 登录'
    },
    component: () => import('@/views/login.vue')
};

export const page404 = {
    path: '/*',
    name: 'error-404',
    meta: {
        title: '404-页面不存在'
    },
    component: () => import('@/views/error-page/404.vue')
};

export const page403 = {
    path: '/403',
    meta: {
        title: '403-权限不足'
    },
    name: 'error-403',
    component: () => import('@/views/error-page/403.vue')
};

export const page500 = {
    path: '/500',
    meta: {
        title: '500-服务端错误'
    },
    name: 'error-500',
    component: () => import('@/views/error-page/500.vue')
};

export const locking = {
    path: '/locking',
    name: 'locking',
    component: () => import('@/views/main-components/lockscreen/components/locking-page.vue')
};

// 作为Main组件的子页面展示但是不在左侧菜单显示的路由写在otherRouter里
export const otherRouter = {
    path: '/',
    name: 'otherRouter',
    redirect: '/home',
    component: Main,
    children: [
        { path: 'home', title: {i18n: 'home'}, name: 'home_index', component: () => import('@/views/home/home.vue') },
        { path: 'ownspace', title: '个人中心', name: 'ownspace_index', component: () => import('@/views/own-space/own-space.vue') },
        /* 基础数据管理相关，二级功能，不用配置权限 */
        { path: '/basicdataindex/:id', title: '基础数据', name: 'basicdataindex', component: () => import('@/views/zjyly/basicModule/basicdata/basicDataList.vue') },
        { path: '/updateBasicData/:id/:typeId', title: '修改基础数据', name: 'updateBasicData', component: () => import('@/views/zjyly/basicModule/basicdata/updateBasicData.vue') },
        { path: '/addBasicData/:typeId', title: '添加基础数据', name: 'addBasicData', component: () => import('@/views/zjyly/basicModule/basicdata/addBasicData.vue') },
        /* 医嘱数据管理，二级功能，不用配置权限 */
        // { path: 'addrole/:id', title: '角色新增', name: 'addrole', component: () => import('@/views/zjyly/setrole/addrole.vue') },
        // { path: 'editrole/:id', title: '角色编辑', name: 'editrole', component: () => import('@/views/zjyly/setrole/editrole.vue') },
        // { path: 'addfunction', title: '功能新增', name: 'addfunction', component: () => import('@/views/zjyly/setfunction/components/addfunction.vue') },
        // { path: 'editfunction/:id', title: '功能编辑', name: 'editfunction', component: () => import('@/views/zjyly/setfunction/components/editfunction.vue') },
        // { path: 'rolefunction/:id', title: '功能设置', name: 'rolefunction', component: () => import('@/views/zjyly/setrole/components/role-function.vue') },
        // { path: 'userrole/:id', title: '角色设置', name: 'userrole', component: () => import('@/views/zjyly/setuser/components/user-role.vue') }
        //{ path: 'editrole/:id', title: '角色编辑', name: 'editrole', component: () => import('@/views/zjyly/setrole/editrole.vue') }

        { path: 'addUser/:id', title: '用户新增', name: 'addUser', component: () => import('@/views/zjyly/setuser/addUser.vue') },
        { path: 'editUser/:id', title: '审核页面', name: 'editUser', component: () => import('@/views/zjyly/setuser/editUser.vue') },

        { path: 'personInfo/:id', title: '个人详情', name: 'personInfo', component: () => import('@/views/zjyly/auditpersonal/personInfo.vue') },
        { path: 'auditPersonInfo/:id', title: '审核页面', name: 'auditPersonInfo', component: () => import('@/views/zjyly/auditpersonal/auditPersonInfo.vue') },
        { path: 'addpersonal/:id', title: '个人认证新增', name: 'addpersonal', component: () => import('@/views/zjyly/auditpersonal/addpersonal.vue') },
        { path: 'editpersonal/:id', title: '个人认证编辑', name: 'editpersonal', component: () => import('@/views/zjyly/auditpersonal/editpersonal.vue') },

        { path: 'partyInfo/:id', title: '企业详情', name: 'partyInfo', component: () => import('@/views/zjyly/auditparty/partyInfo.vue') },
        { path: 'partyInfo_small_b/:id', title: '企业详情', name: 'partyInfo_small_b', component: () => import('@/views/zjyly/auditparty/partyInfo_small_b.vue') },
        { path: 'partyInfo_big_b/:id', title: '企业详情', name: 'partyInfo_big_b', component: () => import('@/views/zjyly/auditparty/partyInfo_big_b.vue') },
        { path: 'auditPartyInfo/:id', title: '审核页面', name: 'auditPartyInfo', component: () => import('@/views/zjyly/auditparty/auditPartyInfo.vue') },
        { path: 'addparty/:id', title: '企业新增', name: 'addparty', component: () => import('@/views/zjyly/auditparty/addparty.vue') },
        { path: 'editparty/:id', title: '企业编辑', name: 'editparty', component: () => import('@/views/zjyly/auditparty/editparty.vue') },

        { path: 'addbanner/:id', title: '轮播图新增', name: 'addbanner', component: () => import('@/views/zjyly/bannerinfo/addbanner.vue') },
        { path: 'editbanner/:id', title: '轮播图修改', name: 'editbanner', component: () => import('@/views/zjyly/bannerinfo/editbanner.vue') },
        { path: 'bannerinfo/:id', title: '轮播图详情', name: 'bannerinfo', component: () => import('@/views/zjyly/bannerinfo/bannerinfo.vue') },

        { path: 'auditorder/:id', title: '订单详情', name: 'auditorder', component: () => import('@/views/zjyly/order/auditorder.vue') },

        { path: 'addcarsource/:id', title: '车源新增', name: 'addcarsource', component: () => import('@/views/zjyly/carsource/addcarsource.vue') },
        { path: 'editcarsource/:id', title: '车源编辑', name: 'editcarsource', component: () => import('@/views/zjyly/carsource/editcarsource.vue') },
        { path: 'auditcarsource/:id', title: '车源审核', name: 'auditcarsource', component: () => import('@/views/zjyly/carsource/auditcarsource.vue') },

        { path: 'addrealcar/:id', title: '车源新增', name: 'addrealcar', component: () => import('@/views/zjyly/realcar/addrealcar.vue') },
        { path: 'editrealcar/:id', title: '车源编辑', name: 'editrealcar', component: () => import('@/views/zjyly/realcar/editrealcar.vue') },

        { path: 'auditpartypersonal/:id', title: '人员审核', name: 'auditpartypersonal', component: () => import('@/views/zjyly/audit_party_personal/auditpartypersonal.vue') },
        { path: 'partypersonalinfo/:id', title: '数据详情', name: 'partypersonalinfo', component: () => import('@/views/zjyly/audit_party_personal/partypersonalinfo.vue') },
        { path: 'editbankinginfo/:id', title: '修改数据', name: 'editbankinginfo', component: () => import('@/views/zjyly/bankinginfo/editbankinginfo.vue') },
        { path: 'editbankinginfo/:id', title: '新增数据', name: 'editbankinginfo', component: () => import('@/views/zjyly/bankinginfo/editbankinginfo.vue') },

        { path: 'addinterfacecar/:id', title: '接口车源新增', name: 'addinterfacecar', component: () => import('@/views/zjyly/interfacecar/addinterfacecar.vue') },
        { path: 'editinterfacecar/:id', title: '接口车源修改', name: 'editinterfacecar', component: () => import('@/views/zjyly/interfacecar/editinterfacecar.vue') },
        { path: 'auditinterfacecar/:id', title: '接口车源审核', name: 'auditinterfacecar', component: () => import('@/views/zjyly/interfacecar/auditinterfacecar.vue') },

        { path: 'addappversion/:id', title: '新增版本', name: 'addappversion', component: () => import('@/views/zjyly/appversion/addappversion.vue') },
        { path: 'editappversion/:id', title: '版本修改', name: 'editappversion', component: () => import('@/views/zjyly/appversion/editappversion.vue') },
        { path: 'appversioninfo/:id', title: '版本详情', name: 'appversioninfo', component: () => import('@/views/zjyly/appversion/appversioninfo.vue') },

        { path: 'adddictype/:id', title: '新增类型', name: 'adddictype', component: () => import('@/views/zjyly/dic/adddictype.vue') },
        { path: 'editdictype/:id', title: '类型修改', name: 'editdictype', component: () => import('@/views/zjyly/dic/editdictype.vue') },
        { path: 'dicdataindex/:id', title: '数据列表', name: 'dicdataindex', component: () => import('@/views/zjyly/dic/dicdataindex.vue') },
        { path: 'adddicdata/:id', title: '新增数据', name: 'adddicdata', component: () => import('@/views/zjyly/dic/adddicdata.vue') },
        { path: 'editdicdata/:id', title: '数据修改', name: 'editdicdata', component: () => import('@/views/zjyly/dic/editdicdata.vue') },

        { path: 'setintentionalorder/:id', title: '意向订单设置', name: 'setintentionalorder', component: () => import('@/views/zjyly/intentional_order/setintentionalorder.vue') },
        { path: 'checkcarorder/:id', title: '验车订单', name: 'checkcarorder', component: () => import('@/views/zjyly/intentional_order/checkcarorder.vue') },
        { path: 'signcontract/:id', title: '签约订单', name: 'signcontract', component: () => import('@/views/zjyly/intentional_order/signcontract.vue') },
        { path: 'finishorder/:id', title: '完成订单', name: 'finishorder', component: () => import('@/views/zjyly/intentional_order/finishorder.vue') },
        { path: 'cancelorder/:id', title: '取消订单', name: 'cancelorder', component: () => import('@/views/zjyly/intentional_order/cancelorder.vue') },

        { path: 'addlogisticstemp/:id', title: '新增类型', name: 'addlogisticstemp', component: () => import('@/views/zjyly/logisticstemp/addlogisticstemp.vue') },
        { path: 'editlogisticstemp/:id', title: '新增类型', name: 'editlogisticstemp', component: () => import('@/views/zjyly/logisticstemp/editlogisticstemp.vue') },

        { path: 'addorderv2/:id', title: '添加订单', name: 'addorderv2', component: () => import('@/views/zjyly/intentional_order_v2/addorderv2.vue') },
        { path: 'editorderv2/:id', title: '修改订单', name: 'editorderv2', component: () => import('@/views/zjyly/intentional_order_v2/editorderv2.vue') },
        { path: 'finishorderv2/:id', title: '订单详情', name: 'finishorderv2', component: () => import('@/views/zjyly/intentional_order_v2/finishorderv2.vue') },
        { path: 'checkcarorderv2/:id', title: '验车信息', name: 'checkcarorderv2', component: () => import('@/views/zjyly/intentional_order_v2/checkcarorderv2.vue') },

        // 车辆信息
        { path: 'vehicleinfo', title: '意向订单管理', icon: 'lock-combination', name: 'vehicleinfo', component: () => import('@/views/zjyly/vehicle_info/vehicleinfo.vue')},
        // 添加车辆
        { path: 'vehicleinfo/:id', title: '添加车辆', name: 'addvehicle', component: () => import('@/views/zjyly/vehicle_info/addvehicle.vue') },

        // 付款凭证
        { path: 'voucher/:id', title: '意向订单管理', icon: 'lock-combination', name: 'voucher', component: () => import('@/views/zjyly/payment_voucher/voucher.vue')},

        // 购车交易查询详情
        { path: 'car_buying_detail/:id', title: '购车交易查询详情', name: 'car_buying_detail', component: () => import('@/views/zjyly/credit_management/car_buying_detail.vue') },


        // 购车尾款确认详情
        { path: 'balance_payment_detail/:id', title: '购车尾款确认详情', name: 'balance_payment_detail', component: () => import('@/views/zjyly/car_balance_payment/balance_payment_detail.vue') },

        //物流公司模块相关
        { path: 'addcompany/:id', title: '新增物流公司', name: 'addcompany', component: () => import('@/views/zjyly/transport_company/addcompany.vue') },
        { path: 'editcompany/:id', title: '修改物流公司', name: 'editcompany', component: () => import('@/views/zjyly/transport_company/editcompany.vue') },
        { path: 'companyinfo/:id', title: '物流公司详情', name: 'companyinfo', component: () => import('@/views/zjyly/transport_company/companyinfo.vue') },

        // 付款交易复核详情
        { path: 'transaction_review_detail/:id', title: '付款交易复核详情', name: 'transaction_review_detail', component: () => import('@/views/zjyly/transaction_review/transaction_review_detail.vue') },

        // 付款交易确认详情
        { path: 'transaction_confirm_detail/:id', title: '付款交易确认详情', name: 'transaction_confirm_detail', component: () => import('@/views/zjyly/transaction_confirm/transaction_confirm_detail.vue') },

        // 付款交易查询详情
        { path: 'transaction_search_detail/:id', title: '付款交易查询详情', name: 'transaction_search_detail', component: () => import('@/views/zjyly/transaction_search/transaction_search_detail.vue') },

        // 处理订单查看详情
        { path: 'addhandleorder/:id', title: '订单新增', name: 'addhandleorder', component: () => import('@/views/zjyly/transport_order_handle/addhandleorder.vue') },

        // 物流订单处理详情
        {path: 'orderhandle_detail/:id', title: '物流订单处理详情', icon: 'lock-combination', name: 'orderhandle_detail', component: () => import('@/views/zjyly/transport_order_handle/orderhandle_detail.vue')},

        // 物流订单处理详情-已取消
        {path: 'ordercancel_detail/:id', title: '物流订单处理详情', icon: 'lock-combination', name: 'ordercancel_detail', component: () => import('@/views/zjyly/transport_order_cancel/ordercancel_detail.vue')},

        // 物流订单查询详情
        {path: 'ordercheck_detail/:id', title: '物流订单处理详情', icon: 'lock-combination', name: 'ordercheck_detail', component: () => import('@/views/zjyly/transport_order_check/ordercheck_detail.vue')},

        // 电子签详情
        { path: 'electronicdetail/:id', title: '电子签详情',icon: 'lock-combination',  name: 'electronicdetail', component: () => import('@/views/zjyly/electronic_visa/electronicdetail.vue') },

        // 电子签审核
        { path: 'electronicaudit/:id', title: '电子签审核',icon: 'lock-combination',  name: 'electronicaudit', component: () => import('@/views/zjyly/electronic_visa/electronicaudit.vue') },
    ]
};

// 作为Main组件的子页面展示并且在左侧菜单显示的路由写在appRouter里
export const appRouter = [
    // {
    //     path: '/demo',
    //     icon: 'key',
    //     name: 'demo',
    //     title: '临时功能',
    //     component: Main,
    //     children: [
    //        {
    //            path: 'dictionary',
    //            title: '字典管理',
    //            icon: 'lock-combination',
    //            name: 'dictionary',
    //            component: () => import('@/views/zjyly/dictionary/dictionary.vue')
    //        },
    //        {
    //            path: 'dictypeindex',
    //            title: '字典数据管理',
    //            icon: 'lock-combination',
    //            name: 'dictypeindex',
    //            component: () => import('@/views/zjyly/dic/dictypeindex.vue')
    //        },
    //        {
    //            path: 'logisticstempindex',
    //            title: '物流订单管理',
    //            icon: 'lock-combination',
    //            name: 'logisticstempindex',
    //            component: () => import('@/views/zjyly/logisticstemp/logisticstempindex.vue')
    //        },
    //         // 收款管理 - 购车交易查询
    //         // {
    //         //     path: 'car_buying_search',
    //         //     title: '购车交易查询',
    //         //     icon: 'lock-combination',
    //         //     name: 'car_buying_search',
    //         //     component: () => import('@/views/zjyly/credit_management/car_buying_search.vue')
    //         // },
    //         // 收款管理 - 购车尾款确认
    //         // {
    //         //     path: 'balance_payment',
    //         //     title: '购车尾款确认',
    //         //     icon: 'lock-combination',
    //         //     name: 'balance_payment',
    //         //     component: () => import('@/views/zjyly/car_balance_payment/balance_payment.vue')
    //         // },
    //         // 付款管理 - 付款交易复核
    //         // {
    //         //     path: 'transaction_review',
    //         //     title: '付款交易复核',
    //         //     icon: 'lock-combination',
    //         //     name: 'transaction_review',
    //         //     component: () => import('@/views/zjyly/transaction_review/transaction_review.vue')
    //         // },
    //         // 付款管理 - 付款交易确认
    //         // {
    //         //     path: 'transaction_confirm',
    //         //     title: '付款交易确认',
    //         //     icon: 'lock-combination',
    //         //     name: 'transaction_confirm',
    //         //     component: () => import('@/views/zjyly/transaction_confirm/transaction_confirm.vue')
    //         // },
    //         // 付款管理 - 付款交易查询
    //         // {
    //         //     path: 'transaction_search',
    //         //     title: '付款交易查询',
    //         //     icon: 'lock-combination',
    //         //     name: 'transaction_search',
    //         //     component: () => import('@/views/zjyly/transaction_search/transaction_search.vue')
    //         // },
    //         // 物流公司管理
    //         // {
    //         //     path: 'intentionalorderindex',
    //         //     title: '订单管理',
    //         //     icon: 'lock-combination',
    //         //     name: 'intentionalorderindex',
    //         //     component: () => import('@/views/zjyly/intentional_order/intentionalorderindex.vue')
    //         // },
    //         // {
    //         //     path: 'companyindex',
    //         //     title: '物流公司管理',
    //         //     icon: 'lock-combination',
    //         //     name: 'companyindex',
    //         //     component: () => import('@/views/zjyly/transport_company/companyindex.vue')
    //         // },
    //         // 物流订单处理
    //         // {
    //         //     path: 'orderhandleindex',
    //         //     title: '物流订单处理',
    //         //     icon: 'lock-combination',
    //         //     name: 'orderhandleindex',
    //         //     component: () => import('@/views/zjyly/transport_order_handle/orderhandleindex.vue')
    //         // },
    //         // // 物流订单查询
    //         // {
    //         //     path: 'ordercheckindex',
    //         //     title: '物流订单查询',
    //         //     icon: 'lock-combination',
    //         //     name: 'ordercheckindex',
    //         //     component: () => import('@/views/zjyly/transport_order_check/ordercheckindex.vue')
    //         // },
    //         // // 物流订单取消
    //         // {
    //         //     path: 'ordercancelindex',
    //         //     title: '物流订单取消',
    //         //     icon: 'lock-combination',
    //         //     name: 'ordercancelindex',
    //         //     component: () => import('@/views/zjyly/transport_order_cancel/ordercancelindex.vue')
    //         // },

    //     ]
    // },

    // {
    //     path: '/demo',
    //     icon: 'key',
    //     name: 'demo',
    //     title: '临时功能',
    //     component: Main,
    //     children: [
    //         {
    //             path: 'searchcarindex',
    //             title: '寻车管理',
    //             icon: 'lock-combination',
    //             name: 'searchcarindex',
    //             component: () => import('@/views/zjyly/search_car/searchcarindex.vue')
    //         }
    //     ]
    // }
];

let localStorage = window.localStorage

if (localStorage.getItem('otherRouter')) {
    let getOtherRouter = JSON.parse(localStorage.getItem('otherRouter'));
    getOtherRouter.forEach(item => {
        console.log(item, '----')
        let comChild = {};
        if (item != null) {
            ensure.forEach(com => {
                comChild[com.name] = com;
            });
            let List = {
                path: item.path,
                title: item.title,
                name: item.name,
                component: comChild[item.name]
            };
           otherRouter.children.push(List);
        }
    });
}
if (localStorage.getItem('appRouter')) {
    let getAppRouter = JSON.parse(localStorage.getItem('appRouter'));
    let comChildObj = {};

    getAppRouter.forEach( index => {

        let faList = {
            path: index.path,
            title: index.title,
            name: index.name,
            component: Main,
            icon: index.icon,
            children: []
        };
        index.children.forEach(a => {
            ensure.forEach(com => {
                comChildObj[com.name] = com;
            });
            let childrenList = {
                path: a.path,
                title: a.title,
                name: a.name,
                component: comChildObj[a.name]
            };
            faList.children.push(childrenList);
        });
        appRouter.push(faList);
    });
}


// 所有上面定义的路由都要写在下面的routers里
export const routers = [
    loginRouter,
    otherRouter,
    locking,
    ...appRouter,
    page500,
    page403,
    page404
];
