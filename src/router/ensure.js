// 基本设置 - 角色管理
export const setroleindex = resolve => { require(['@/views/zjyly/setrole/setrole.vue'], resolve) }
// 基本设置 - 角色管理 - 新增角色
export const addrole = resolve => { require(['@/views/zjyly/setrole/addrole.vue'], resolve) }
// 基本设置 - 角色管理 - 编辑角色
export const editrole = resolve => { require(['@/views/zjyly/setrole/editrole.vue'], resolve) }
// 基本功能 - 角色管理 - 角色功能设置
export const rolefunction = resolve => { require(['@/views/zjyly/setrole/components/role-function.vue'], resolve) }
// 基本设置 - 功能管理
export const setfunctionindex = resolve => { require(['@/views/zjyly/setfunction/setfunction.vue'], resolve) }
// 基本设置 - 功能管理 - 新增功能
export const addfunction = resolve => { require(['@/views/zjyly/setfunction/components/addfunction.vue'], resolve) }
// 基本设置 - 功能管理 - 编辑功能
export const editfunction = resolve => { require(['@/views/zjyly/setfunction/components/editfunction.vue'], resolve) }
// 基本设置 - 成员管理
export const setuserindex = resolve => { require(['@/views/zjyly/setuser/setuser.vue'], resolve) }
// 基本设置 - 成员管理 - 角色设置
export const userrole = resolve => { require(['@/views/zjyly/setuser/components/user-role.vue'], resolve) }

export const dictionary = resolve => { require(['@/views/zjyly/dictionary/dictionary.vue'], resolve) }

export const basictypeindex = resolve => { require(['@/views/zjyly/basicModule/basictype/basicTypeList.vue'], resolve) }
export const addBasicType = resolve => { require(['@/views/zjyly/basicModule/basictype/addBasicType.vue'], resolve) }
export const updateBasicType = resolve => { require(['@/views/zjyly/basicModule/basictype/updateBasicType.vue'], resolve) }

export const personalindex = resolve => { require(['@/views/zjyly/auditpersonal/personalindex.vue'], resolve) }
export const partyindex = resolve => { require(['@/views/zjyly/auditparty/partyindex.vue'], resolve) }
export const partypersonalindex = resolve => { require(['@/views/zjyly/audit_party_personal/partypersonalindex.vue'], resolve)}
export const logisticsindex = resolve => { require(['@/views/zjyly/logistics/logisticsindex.vue'], resolve) }
export const financeindex = resolve => { require(['@/views/zjyly/finance/financeindex.vue'], resolve) }
export const buycarindex = resolve => { require(['@/views/zjyly/userbuycar/buycarindex.vue'], resolve) }

export const bannerindex = resolve => { require(['@/views/zjyly/bannerinfo/bannerindex.vue'], resolve) }
export const orderindex = resolve => { require(['@/views/zjyly/order/orderindex.vue'], resolve) }
export const carsourceindex = resolve => { require(['@/views/zjyly/carsource/carsourceindex.vue'], resolve) }
export const realcarindex = resolve => { require(['@/views/zjyly/realcar/realcarindex.vue'], resolve) }

export const bankinginfoindex = resolve => { require(['@/views/zjyly/bankinginfo/bankinginfoindex.vue'], resolve) }
export const interfacecarindex = resolve => { require(['@/views/zjyly/interfacecar/interfacecarindex.vue'], resolve) }
export const appversionindex = resolve => { require(['@/views/zjyly/appversion/appversionindex.vue'], resolve) }
export const intentionalorderindex = resolve => { require(['@/views/zjyly/intentional_order/intentionalorderindex.vue'], resolve) }
export const orderv2index = resolve => { require(['@/views/zjyly/intentional_order_v2/orderv2index.vue'], resolve) }
export const carbuyingsearch = resolve => { require(['@/views/zjyly/credit_management/car_buying_search.vue'], resolve) }
export const balancepayment = resolve => { require(['@/views/zjyly/car_balance_payment/balance_payment.vue'], resolve) }
export const transactionreview = resolve => { require(['@/views/zjyly/transaction_review/transaction_review.vue'], resolve) }
export const transactionconfirm = resolve => { require(['@/views/zjyly/transaction_confirm/transaction_confirm.vue'], resolve) }
export const transactionsearch = resolve => { require(['@/views/zjyly/transaction_search/transaction_search.vue'], resolve) }
export const companyindex = resolve => { require(['@/views/zjyly/transport_company/companyindex.vue'], resolve) }
export const orderhandleindex = resolve => { require(['@/views/zjyly/transport_order_handle/orderhandleindex.vue'], resolve) }
export const ordercheckindex = resolve => { require(['@/views/zjyly/transport_order_check/ordercheckindex.vue'], resolve) }
export const ordercancelindex = resolve => { require(['@/views/zjyly/transport_order_cancel/ordercancelindex.vue'], resolve) }
export const electronicindex = resolve => { require(['@/views/zjyly/electronic_visa/electronicindex.vue'], resolve) }
export const searchcarindex = resolve => { require(['@/views/zjyly/search_car/searchcarindex.vue'], resolve) }

export const ensure = [
    setroleindex,
    setfunctionindex,
    setuserindex,
    addrole,
    editrole,
    addfunction,
    editfunction,
    rolefunction,
    userrole,
    dictionary,
    basictypeindex,
    addBasicType,
    updateBasicType,
    personalindex,
    partyindex,
    logisticsindex,
    financeindex,
    buycarindex,
    bannerindex,
    orderindex,
    carsourceindex,
    realcarindex,
    partypersonalindex,
    bankinginfoindex,
    interfacecarindex,
    appversionindex,
    intentionalorderindex,
    orderv2index,
    carbuyingsearch,
    balancepayment,
    transactionreview,
    transactionconfirm,
    transactionsearch,
    companyindex,
    orderhandleindex,
    ordercheckindex,
    ordercancelindex,
    electronicindex,
    searchcarindex
]
